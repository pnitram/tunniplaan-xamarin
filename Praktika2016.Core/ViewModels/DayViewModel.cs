﻿using MvvmCross.Core.ViewModels;
using Praktika2016.Core.Models;
using System.Collections.ObjectModel;

namespace Praktika2016.Core.ViewModels
{
    public class DayViewModel : MvxViewModel
    {
        public DayViewModel(string title)
        {
            Title = title;
            Date = "";
            Items = new ObservableCollection<ListItem>();
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { _title = value; RaisePropertyChanged(() => Title); }
        }

        private string _date;
        public string Date
        {
            get { return _date; }
            set { _date = value; RaisePropertyChanged(() => Date); }
        }

        private ObservableCollection<ListItem> _items;
        public ObservableCollection<ListItem> Items
        {
            get { return _items; }
            set { _items = value; RaisePropertyChanged(() => Items); }
        }
    }
}
