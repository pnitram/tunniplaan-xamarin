using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.Core;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Praktika2016.Core.Helpers;
using Praktika2016.Core.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Praktika2016.Core.ViewModels
{
    public class FirstViewModel : MvxViewModel
    {
        private bool _isBusy;
        private string _viimane_muudatus;
        private string _groupname;
        private DateTime _currentWeek;
        private SortedDictionary<string, int> _groups;
        private Dictionary<string, List<Tund>> _tunnid;
        private Response _jsonResponse;
        private List<string> _weekDays;

        public event EventHandler afterJson;

        public FirstViewModel()
        {
            //_currentWeek = getWeek();
            _currentWeek = DateTime.Parse("2016-06-06"); // testimise jaoks
            _weekDays = new List<string>();

            for (int i = 0; i < 5; i++)
            {
                _weekDays.Add(_currentWeek.AddDays(i).ToString("yyyy-MM-dd"));
            }

            Groups = new SortedDictionary<string, int>{
                                                        { "TT-14E", 136 },
                                                        { "TT-14T", 137 },
                                                        { "MM-14", 138 },
                                                        { "EA-14", 139 },
                                                        { "EV-14", 140 },
                                                        { "SA-14", 141 },
                                                        { "AA-14", 142 },
                                                        { "AV-14", 143 },
                                                        { "IT-14E", 147 },
                                                        { "IT-14V", 148 },
                                                        { "TA-14", 149 },
                                                        { "AA-15", 164 },
                                                        { "AV-15", 165 },
                                                        { "SA-15", 166 },
                                                        { "MM-15", 167 },
                                                        { "IT-15E", 169 },
                                                        { "IT-15V", 170 },
                                                        { "TA-15", 171 },
                                                        { "KEE-15", 183 },
                                                        };


            GroupName = Groups.FirstOrDefault(x => x.Value == int.Parse(Settings.GroupId)).Key;

            Days.Add("E", "Esmasp�ev");
            Days.Add("T", "Teisip�ev");
            Days.Add("K", "Kolmap�ev");
            Days.Add("N", "Neljap�ev");
            Days.Add("R", "Reede");

            for (int i = 0; i < 5; i++)
            {
                DayViewModelsList.Add(new DayViewModel(Days.ElementAt(i).Key));
            }

            //http://stackoverflow.com/questions/22087005/task-factory-startnew-vs-new-task
            //Task t = new Task(DownloadPageAsync);
            Task.Run(() => DownloadPageAsync());
        }

        /// <summary>
        /// T�mbab JSON-i, paneb selle objekt.
        /// </summary>
        async void DownloadPageAsync()
        {
            if (!IsConnected)
            {
                return;
            }

            //K�ivitab Progress Bari
            IsBusy = true;
            string week = _currentWeek.Date.ToString("yyyy-MM-dd");
            string groupID = Settings.GroupId;

            var client = new HttpClient();

            var response = await client.GetAsync("http://knjs.xyz:3000/tunniplaan?nadal=" + week + "&grupp=" + groupID);
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();

                try
                {
                    populateWithJson(JsonConvert.DeserializeObject<Response>(jsonString));
                }
                catch (Exception e)
                {
                    populateWithJson(new Response());
                }

                IsBusy = false;
                afterJson?.Invoke(this, null);
            }
        }

        public Dictionary<string, string> Days = new Dictionary<string, string>();
        public List<DayViewModel> DayViewModelsList = new List<DayViewModel>();

        public void populateWithJson(Response json)
        {
            JsonResponse = json;

            var dispatcher = Mvx.Resolve<IMvxMainThreadDispatcher>();
            dispatcher.RequestMainThreadAction(() =>
            {
                Tunnid = JsonResponse.tunnid ?? new Dictionary<string, List<Tund>>();
                Viimane_Muudatus = JsonResponse.viimane_muudatus;

                int j = 0;
                foreach (var child in DayViewModelsList)
                {
                    child.Date = formatDate(_weekDays.ElementAt(j));
                    child.Items.Clear();
                    j++;
                }

                int i = 0;
                foreach (var day in _weekDays)
                {
                    foreach (var tund in Tunnid)
                    {
                        foreach (var tundinfo in tund.Value)
                        {
                            if (day == tund.Key)
                            {
                                DayViewModelsList.ElementAt(i).Items.Add(new ListItem { Aine = tundinfo.aine, Kestus = tundinfo.algus + " - " + tundinfo.lopp, Opetaja = tundinfo.opetaja, Ruum = tundinfo.ruum });
                            }
                        }
                    }
                    i++;
                }

                foreach (var dayVM in DayViewModelsList)
                {
                    if (dayVM.Items.Count == 0)
                    {
                        dayVM.Items.Add(new ListItem { Aine = "Tunde pole", Kestus = null, Opetaja = null, Ruum = null });
                    }
                }
            });
        }

        public Response JsonResponse
        {
            get { return _jsonResponse; }
            set { SetProperty(ref _jsonResponse, value); }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set { _isBusy = value; RaisePropertyChanged(() => IsBusy); }
        }

        public string Viimane_Muudatus
        {
            get { return _viimane_muudatus; }
            set { SetProperty(ref _viimane_muudatus, value); }
        }

        public SortedDictionary<string, int> Groups
        {
            get { return _groups; }
            set { SetProperty(ref _groups, value); }
        }

        public string GroupName
        {
            get { return _groupname; }
            set { SetProperty(ref _groupname, value); }
        }

        public Dictionary<string, List<Tund>> Tunnid
        {
            get { return _tunnid; }
            set { SetProperty(ref _tunnid, value); }
        }

        ICommand _goRefreshTimetableCommand;
        public ICommand goRefreshTimetableCommand
        {
            get
            {
                Debug.WriteLine("Refreshing");
                _goRefreshTimetableCommand = _goRefreshTimetableCommand ?? new MvxCommand(DoGoRefreshTimetableCommand);
                return _goRefreshTimetableCommand;
            }
        }
        private void DoGoRefreshTimetableCommand()
        {
            Task.Run(() => DownloadPageAsync());
        }

        private string formatDate(string dateValue)
        {
            string pattern = "yyyy-MM-dd";
            DateTime parsedDate;

            DateTime.TryParseExact(dateValue, pattern, null, DateTimeStyles.None, out parsedDate);

            return parsedDate.ToString("ddd dd.MM");
        }

        private DateTime getWeek()
        {
            DateTime dt = DateTime.Now;
            var finalDate = dt;
            int diff = dt.DayOfWeek - DayOfWeek.Monday;
            if (diff < 0)
            {
                diff += 7;
            }
            
            if (dt.DayOfWeek == DayOfWeek.Saturday)
            {
                finalDate = finalDate.AddDays(2);
            }
            else if(dt.DayOfWeek == DayOfWeek.Sunday)
            {
                finalDate = finalDate.AddDays(1);
            }
            else
            {
                finalDate = finalDate.AddDays(-1 * diff);
            }

            return finalDate;
        }

        public bool IsConnected
        {
            get { return CrossConnectivity.Current.IsConnected; }
        }
    }
}
