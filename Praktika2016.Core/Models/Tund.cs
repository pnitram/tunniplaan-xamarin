﻿namespace Praktika2016.Core.Models
{
    public struct Tund
    {
        public string tund { get; set; }
        public string algus { get; set; }
        public string lopp { get; set; }
        public string aine { get; set; }
        public string grupp { get; set; }
        public string opetaja { get; set; }
        public string ruum { get; set; }
    }
}
