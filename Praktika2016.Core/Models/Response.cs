﻿using System.Collections.Generic;

namespace Praktika2016.Core.Models
{
    public struct Response
    {
        public string nadal { get; set; }
        public Dictionary<string, string> ajad { get; set; }
        public Dictionary<string, List<Tund>> tunnid { get; set; }
        public string viimane_muudatus { get; set; }
    }
}
