﻿namespace Praktika2016.Core.Models
{
    public struct ListItem
    {
        public string Aine { get; set; }
        public string Kestus { get; set; }
        public string Grupp { get; set; }
        public string Opetaja { get; set; }
        public string Ruum { get; set; }
    }
}
