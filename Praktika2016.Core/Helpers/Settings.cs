// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Praktika2016.Core.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    ///    
    public static class Settings
    {
        private const string GroupIdKey = "groupid_key";
        private static readonly string GroupIdDefault = "149";

        public static string GroupId
        {
            get { return AppSettings.GetValueOrDefault<string>(GroupIdKey, GroupIdDefault); }
            set { AppSettings.AddOrUpdateValue<string>(GroupIdKey, value); }
        }

        private static ISettings AppSettings
        {
            get { return CrossSettings.Current; }
        }
    }
}