using Android.Content;
using Android.Support.V4.App;
using MvvmCross.Core.ViewModels;
using MvvmCross.Droid.Support.V4;
using System;
using System.Collections.Generic;
using System.Linq;
using Fragment = Android.Support.V4.App.Fragment;

namespace Praktika2016.Droid.Views
{
    public class ViewPagerAdapter : FragmentPagerAdapter
    {
        private readonly Context _context;

        public ViewPagerAdapter(Context context, Android.Support.V4.App.FragmentManager fragmentManager, IEnumerable<FragmentInfo> fragments) : base(fragmentManager)
        {
            _context = context;
            Fragments = fragments;
        }

        public IEnumerable<FragmentInfo> Fragments
        {
            get;
            private set;
        }

        public override int Count
        {
            get { return Fragments.Count(); }
        }

        public override Fragment GetItem(int position)
        {
            var frag = Fragments.ElementAt(position);
            var fragment = Fragment.Instantiate(_context, Java.Lang.Class.FromType(frag.FragmentType).Name);

            ((MvxFragment)fragment).DataContext = frag.ViewModel;
            return fragment;
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int p0)
        {
            return new Java.Lang.String(Fragments.ElementAt(p0).Title);
        }

        public class FragmentInfo
        {
            public string Title { get; set; }
            public Type FragmentType { get; set; }
            public IMvxViewModel ViewModel { get; set; }
        }
    }
}