using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Views;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.AppCompat;
using Newtonsoft.Json;
using Praktika2016.Core.Helpers;
using Praktika2016.Core.Models;
using Praktika2016.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Praktika2016.Droid.Views
{
    [Activity(Label = "View for FirstViewModel", ScreenOrientation = ScreenOrientation.Portrait)]
    public class FirstView : MvxAppCompatActivity
    {
        //Declaring All The Variables Needed

        private TabLayout tabLayout;
        private ViewPager viewPager;
        private ViewPagerAdapter viewPagerAdapter;

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.home, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.ItemId;

            //noinspection SimplifiableIfStatement
            
            if (id == Resource.Id.menu_groupname)
            {
                showGroups();
                return true;
            }
            if (id == Resource.Id.menu_refresh)
            {
                refreshTimeTable();
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private BindableProgress _bindableProgress;

        protected FirstViewModel FirstViewModel
        {
            get { return base.ViewModel as FirstViewModel; }
        }


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            if (Build.VERSION.SdkInt >= BuildVersionCodes.Lollipop)
            {
                //See lubab status bari v�rvi muuta.
                this.Window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
            }

            SetContentView(Resource.Layout.FirstView);

            hasInternet();

            var fragments = new List<ViewPagerAdapter.FragmentInfo>();
            int i = 0;
            foreach (var dayFragment in FirstViewModel.DayViewModelsList)
            {
                fragments.Add(new ViewPagerAdapter.FragmentInfo { FragmentType = typeof(TabFragment), Title = dayFragment.Title, ViewModel = dayFragment });
                i++;
            }

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            tabLayout = FindViewById<TabLayout>(Resource.Id.tabs);
            viewPager = FindViewById<ViewPager>(Resource.Id.viewpager);
            viewPagerAdapter = new ViewPagerAdapter(this, SupportFragmentManager, fragments);
            viewPager.Adapter = viewPagerAdapter;

            tabLayout.SetupWithViewPager(viewPager);

            SetSupportActionBar(toolbar);
            SupportActionBar.Title = GetString(Resource.String.timetable)+" - " + FirstViewModel.Groups.FirstOrDefault(x => x.Value == int.Parse(Settings.GroupId)).Key;
            _bindableProgress = new BindableProgress(this);

            var set = this.CreateBindingSet<FirstView, FirstViewModel>();
            set.Bind(_bindableProgress).For(p => p.Visible).To(vm => vm.IsBusy);
            set.Apply();

            if (!FirstViewModel.IsConnected)
            {
                string json = "";
                string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string filePath = Path.Combine(path, "timetable.json");
                if (File.Exists(filePath))
                {
                    using (var file = File.Open(filePath, FileMode.Open, FileAccess.Read))
                    {
                        using (var sr = new StreamReader(file))
                        {
                            json = sr.ReadToEnd();
                        }
                        FirstViewModel.populateWithJson(JsonConvert.DeserializeObject<Response>(json));
                    }
                }
            }

            FirstViewModel.afterJson += FirstViewModel_afterJson;
        }

        private void FirstViewModel_afterJson(object sender, EventArgs e)
        {
            var json = JsonConvert.SerializeObject(FirstViewModel.JsonResponse);
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filePath = Path.Combine(path, "timetable.json");
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            using (var file = File.Open(filePath, FileMode.Create, FileAccess.Write))
            {
                using (var sw = new StreamWriter(file))
                {
                    sw.Write(json);
                }
            }
        }

        private void refreshTimeTable()
        {
            if (hasInternet())
            {
                FirstViewModel.goRefreshTimetableCommand.Execute(null);
            }
        }

        private bool hasInternet()
        {
            if (FirstViewModel.IsConnected)
            {
                return true;
            }
            else
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle(GetString(Resource.String.nointernet));
                alert.SetNeutralButton("OK", (sender, args) =>
                {
                    alert.Dispose();
                });

                RunOnUiThread(() =>
                {
                    alert.Show();
                });
                return false;
            }
        }

        private void showGroups()
        {
            AlertDialog.Builder b = new AlertDialog.Builder(this);
            b.SetTitle(GetString(Resource.String.group));
            b.SetItems(FirstViewModel.Groups.Keys.ToArray(), ListClicked);

            RunOnUiThread(() => {
                b.Show();
            });
        }

        private void ListClicked(object sender, DialogClickEventArgs e)
        {
            if (hasInternet())
            {
                Settings.GroupId = FirstViewModel.Groups.ElementAt(e.Which).Value.ToString();
                RunOnUiThread(() =>
                {
                    SupportActionBar.Title = GetString(Resource.String.timetable)+" - " + FirstViewModel.Groups.FirstOrDefault(x => x.Value == int.Parse(Settings.GroupId)).Key;
                });
                refreshTimeTable();
            }
        }
    }
}